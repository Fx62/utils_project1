package com.byteslounge.websockets;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/websocket")
public class WebSocketTest {

    private static Set<Session> clients
            = Collections.synchronizedSet(new HashSet<Session>());
    private String players = "<b>Todos los juagdores se encuentran conectados</b>\n";

    @OnMessage
    public void onMessage(String message, Session session)
            throws IOException {

        synchronized (clients) {
            // Iterate over the connected sessions
            // and broadcast the received message
            for (Session client : clients) {
                //if (!client.equals(session)){
                client.getBasicRemote().sendText(message);
                //}
            }
        }

    }

    public String playersConnected() {
        switch (clients.size()) {
            case 1:
                players += "Jugador 1 conectado\n";
                break;
            case 2:
                players += "Jugador 1 y Jugador 2 conectados\n";
                break;
            case 3:
                players += "Jugador 1,  Jugador 2 y Jugador 3 conectados\n";
                break;
        }
        return players;
    }

    @OnOpen
    public void onOpen(Session session) throws IOException {
        if (clients.size() != 3) {
            clients.add(session);
            synchronized (clients) {
                for (Session client : clients) {
                    //if (client == session) {
                    client.getBasicRemote().sendText(new Date().toString());
                }
            }
        }
    }

    @OnClose
    public void onClose(Session session) {
        // Remove session from the connected sessions set
        clients.remove(session);
    }

}
