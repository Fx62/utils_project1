
import java.io.IOException;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/clock")
public class WebSocketClock { 
    
  ScheduledExecutorService timer = 
       Executors.newSingleThreadScheduledExecutor(); 
 
  private static Set<Session> allSessions; 
 
  @OnOpen  
  public void showTime(Session session){
      allSessions = session.getOpenSessions();
      System.out.println(allSessions.size());
      allSessions.add(session);
      System.out.println(allSessions.size());
      
      // start the scheduler on the very first connection
      // to call sendTimeToAll every second   
        timer.scheduleAtFixedRate(
             () -> sendTimeToAll(session),0,3,TimeUnit.SECONDS); 
     }      
 
   private void sendTimeToAll(Session session){       
     //allSessions = session.getOpenSessions();
     allSessions.forEach((sess) -> {
         try{
             sess.getBasicRemote().sendText(new Date().toString());
         } catch (IOException ioe) {
             System.out.println("\n******" + ioe.getMessage());
         }
      });   
  }
}